////////////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022////////
/////// @file updateCats.h
/////// @version 1.0 - Initial version
///////
/////// This module will have three functions: updateCatName, fixCat, updateCatWeight
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   15_Mar_2022
///////////////////////////////////////////////////////////////////////////////////////
//
#pragma once

extern int updateCatName(const int index, const char newName[]);
extern int fixCat(const int index);
extern int updateCatWeight(const int index, const float newWeight);

extern int isIndexValid(const int index);
extern int updateCatCollar1(const int index, const unsigned int newColor1);
extern int updateCatCollar2(const int index, const unsigned int newColor2);
extern int updateCatLicense(const int index, const unsigned long long license);
