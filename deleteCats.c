//////////////////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022////////
/////// @file deleteCats.c
/////// @version 1.0 - Initial version
///////
/////// This module will have one function deleteAllCats() and an optional deleteCat(index)
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   15_Mar_2022
/////////////////////////////////////////////////////////////////////////////////////////
//
#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats(){
   numberOfCats = 0;
}
