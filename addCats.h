///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file addCats.h
/////// @version 1.0 - Initial version
///////
/////// This module will add cats to the database. The module only has one
/////// function
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   15_Mar_2022
///////////////////////////////////////////////////////////////////////////////
//
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#include <stdbool.h>

extern int addCat(const char name[], const int gender, const int breed, const bool isFixed, const float weight, const unsigned int collarColor1, const unsigned int collarColor2, const unsigned long long license);
