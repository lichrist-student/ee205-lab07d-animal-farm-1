///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file addCats.c
/////// @version 1.0 - Initial version
///////
/////// This module will add cats to the database. The module only has one
/////// function
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   15_Mar_2022
//////////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#include "addCats.h"
#include "config.h"

int addCat(const char name[], const int gender, const int breed, const bool isFixed, const float weight, const unsigned int collarColor1, const unsigned int collarColor2, const unsigned long long license){

   if(numberOfCats > MAXIMUM_CAT){
      fprintf(stderr, "%s Error: Database is full!\n", PROGRAM_NAME);
      return 1;
   }
   if(strlen(name) <= 0){
      fprintf(stderr, "%s Error: The cat's name is empty!\n", PROGRAM_NAME);
      return 1;
   }
   if(strlen(name) > MAXIMUM_CHAR_CAT){
      fprintf(stderr, "%s Error: The cat's name is greater than 30 characters!\n", PROGRAM_NAME);
      return 1;
   }
   if(weight <= 0){
      fprintf(stderr, "%s Error: Weight must be greater than 0!\n", PROGRAM_NAME);
      return 1;
   }
   if(collarColor1 == collarColor2){
      fprintf(stderr, "%sError: Both collars cannot be the same color!\n", PROGRAM_NAME);
      return 1;
   }

   for(int i=0; i<numberOfCats; i++){
      if(strcmp(cats[i].name, name) == 0){
         fprintf(stderr, "%s Error: Name must be unique!\n", PROGRAM_NAME);
         return 1;
      }
      if(license == cats[i].license){
         fprintf(stderr, "%s Error: License must be unique!\n", PROGRAM_NAME);
         return 1;
      }
      if((collarColor1 == cats[i].collarColor1) && (collarColor1 == cats[i].collarColor2)){
         fprintf(stderr, "%s Error: collarColor must be unique!\n", PROGRAM_NAME);
         return 1;
      }
   }



strcpy(cats[numberOfCats].name ,name);
cats[numberOfCats].gender = gender;
cats[numberOfCats].breed = breed;
cats[numberOfCats].isFixed = isFixed;
cats[numberOfCats].weight = weight;
cats[numberOfCats].collarColor1 = collarColor1;
cats[numberOfCats].collarColor2 = collarColor2;
cats[numberOfCats].license = license;

numberOfCats++;
return numberOfCats;
}
