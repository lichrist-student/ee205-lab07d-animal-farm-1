///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file reportCats.h
/////// @version 1.0 - Initial version
///////
/////// This module will have three functions: printCat, printAllCats, findCat
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   15_Mar_2022
//////////////////////////////////////////////////////////////////////////////////////
//
#pragma once

extern void printCat(const int index);
extern void printAllCats();
extern int findCat(const char name[]);
